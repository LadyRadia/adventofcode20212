use crate::error::SolutionError;
use std::fmt::Debug;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

pub trait Solution {
    fn solve(&self) -> Result<u64, SolutionError>;
}

pub fn read_input<T>(filename: &str) -> Result<Vec<T>, SolutionError>
where
    T: FromStr,
{
    match File::open(filename) {
        Ok(file) => {
            let buf_reader = BufReader::new(file);
            buf_reader
                .lines()
                .map(|l| {
                    l.map_err(|e| SolutionError {
                        reason: format!("{}", e),
                    })
                    .and_then(|s| {
                        s.parse::<T>().map_err(|e| SolutionError {
                            reason: format!("{:?}", "Error parsing type"),
                        })
                    })
                })
                .collect()
        }

        Err(e) => Err(SolutionError {
            reason: format!("Unable to open file: {}", e),
        }),
    }
}

pub fn binary_string_to_u64(binary: &str) -> Result<u64, SolutionError> {
    return u64::from_str_radix(binary, 2).map_err(|e| SolutionError {
        reason: e.to_string(),
    });
}
