use crate::utility::Solution;

mod Day01;
mod Day02;
mod Day03;
mod error;
mod utility;
fn separate() {
    println!("------------------------");
}

fn main() {
    let solutions: Vec<Box<dyn Solution>> = vec![
        Box::new(Day01::Day1Solution {}),
        Box::new(Day02::Day2Solution {}),
        Box::new(Day03::Day3Solution {}),
    ];
    let mut i = 1;
    for solution in solutions.iter() {
        separate();
        println!("Executing Day {}", i);
        match solution.solve() {
            Ok(solution_val) => {
                println!("Success! Result is {}", solution_val)
            }
            Err(e) => println!("Failure! Result is {}", e.reason),
        }
        separate();
        i += 1;
    }
}
