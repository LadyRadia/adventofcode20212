use crate::error::SolutionError;
use crate::utility::read_input;
use crate::Solution;
use std::str::{FromStr, Split};

const FILENAME: &str = "inputs/Day02.txt";

enum SubVector {
    Up(u64),
    Down(u64),
    Forward(u64),
}

impl FromStr for SubVector {
    type Err = SolutionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(" ").collect();
        let numeric_val = split[1].parse().map_err(|e| SolutionError {
            reason: format!(
                "Unable to parse number as second line of input. {}",
                split[1]
            ),
        })?;

        let retVal = match split[0] {
            "forward" => Self::Forward(numeric_val),
            "up" => Self::Up(numeric_val),
            "down" => Self::Down(numeric_val),
            _ => {
                return Err(SolutionError {
                    reason: format!("Unrecognized type found for vector: {}", split[0]),
                })
            }
        };
        return Ok(retVal);
    }
}

pub struct Day2Solution {}
impl Day2Solution {
    fn solve_part_one() -> Result<u64, SolutionError> {
        let movements: Vec<SubVector> = read_input(FILENAME)?;
        let mut depth: i64 = 0;
        let mut horiz_position = 0;
        for vec in movements {
            match vec {
                SubVector::Forward(mov) => horiz_position += mov,
                SubVector::Up(mov) => depth -= mov as i64,
                SubVector::Down(mov) => depth += mov as i64,
            }
        }
        Ok((depth * horiz_position as i64) as u64)
    }
}
impl Solution for Day2Solution {
    fn solve(&self) -> Result<u64, SolutionError> {
        let movements: Vec<SubVector> = read_input(FILENAME)?;
        let mut depth: i64 = 0;
        let mut horiz_position = 0;
        let mut aim: i64 = 0;
        for vec in movements {
            match vec {
                SubVector::Forward(mov) => {
                    horiz_position += mov;
                    depth += (aim * mov as i64)
                }
                SubVector::Up(mov) => aim -= mov as i64,
                SubVector::Down(mov) => {
                    aim += mov as i64;
                }
            }
        }
        Ok((depth * horiz_position as i64) as u64)
    }
}
