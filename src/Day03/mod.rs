//    01001
// *  10110
// 11000110
//works just like by hand decimal mult, so.. eh, this didnt REALLY give us a shortcut :)
//woulda been nice tho.

use crate::error::SolutionError;
use crate::utility::{binary_string_to_u64, read_input};
use crate::Solution;

const FILENAME: &str = "inputs/Day03.txt";

pub struct Day3Solution {}

impl Day3Solution {
    fn solve_part_1(&self) -> Result<u64, SolutionError> {
        let raw_bin_values: Vec<String> = read_input(FILENAME)?;
        let full_len = raw_bin_values.len();
        let first = raw_bin_values.first().ok_or_else(|| SolutionError {
            reason: format!("Not at least one input value found."),
        })?;
        //determine the number of columns so we can create a vector of our counts.
        let num_cols = first.len();
        //...TECHNICALLY looking at the inputs it's just a byte (8 bits) for this problem,
        //but i'll be DAMNED if the functions i write aren't cool looking and arbitrary-problem-solving.
        let mut num_one_instances: Vec<u64> = vec![0; num_cols];
        for bin_val in raw_bin_values {
            let mut i = 0;
            for bit in bin_val.chars() {
                if bit == '1' {
                    num_one_instances[i] += 1;
                }
                i += 1;
            }
        }
        //sweet, now we can create our individual binary strings.
        let mut gamma_rate_string = String::new();
        let mut epsilon_rate_string = String::new();
        for num_ones in num_one_instances {
            if num_ones > (full_len / 2) as u64 {
                gamma_rate_string.push('1');
                epsilon_rate_string.push('0');
            } else {
                gamma_rate_string.push('0');
                epsilon_rate_string.push('1');
            }
        }
        let gamma_rate = u64::from_str_radix(gamma_rate_string.as_str(), 2).map_err(|e|
            SolutionError {
                reason: format!("Unable to turn Gamma Rate binary string to a number. Something probably went wrong creating the binary string.")
            }
        ) ?;
        let epsilon_rate = u64::from_str_radix(epsilon_rate_string.as_str(), 2).map_err(|e|
            SolutionError {
                reason: format!("Unable to turn Epsilon Rate binary string to a number. Something probably went wrong creating the binary string.")
            }
        ) ?;

        return Ok(gamma_rate * epsilon_rate);
    }
}
///given an array of binary strings,
/// returns two list of binary strings,
/// where the first list is strings where 0 was the value at column,
/// and the second list is strings where 1 was the value at column.
/// Assumes column < string len
fn split_on_common_bits(binary_strings: &Vec<String>, column: usize) -> (Vec<String>, Vec<String>) {
    let mut ones = vec![];
    let mut zeroes = vec![];
    for binary_string in binary_strings {
        if binary_string
            .chars()
            .nth(column)
            .expect("Column size exceeded string size")
            == '1'
        {
            ones.push(binary_string.into());
        } else {
            zeroes.push(binary_string.into());
        }
    }

    return (zeroes, ones);
}

fn reduce_based_on_predicate(
    binary_strings: &Vec<String>,
    column: usize,
    pred: fn(Vec<String>, Vec<String>) -> Vec<String>,
) -> Vec<String> {
    let (zeroes, ones) = split_on_common_bits(&binary_strings, column);

    return pred(zeroes, ones);
}
fn oxy_gen_rate_pred(zeroes: Vec<String>, ones: Vec<String>) -> Vec<String> {
    return if zeroes.len() > ones.len() {
        zeroes.to_vec()
    } else if zeroes.len() < ones.len() {
        ones.to_vec()
    } else {
        ones.to_vec()
    };
}

fn co2_rate_pred(zeroes: Vec<String>, ones: Vec<String>) -> Vec<String> {
    return if zeroes.len() < ones.len() {
        zeroes.to_vec()
    } else if zeroes.len() > ones.len() {
        ones.to_vec()
    } else {
        zeroes.to_vec()
    };
}

impl Solution for Day3Solution {
    fn solve(&self) -> Result<u64, SolutionError> {
        let raw_bin_values: Vec<String> = read_input(FILENAME)?;
        let mut oxy_vals = raw_bin_values.to_vec();
        let mut co2_vals = raw_bin_values.to_vec();
        for i in 0..12 {
            if oxy_vals.len() > 1 {
                oxy_vals = reduce_based_on_predicate(&oxy_vals, i, oxy_gen_rate_pred);
            }
            if co2_vals.len() > 1 {
                co2_vals = reduce_based_on_predicate(&co2_vals, i, co2_rate_pred);
            }
        }
        let oxy_val_raw = oxy_vals.first().unwrap();
        let co2_val_raw = co2_vals.first().unwrap();
        let oxy_val = binary_string_to_u64(oxy_val_raw.as_str())?;
        let co2_val = binary_string_to_u64(co2_val_raw.as_str())?;

        return Ok(oxy_val * co2_val);
    }
}
