use crate::error::SolutionError;
use crate::utility::{read_input, Solution};
use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind};

const FILENAME: &str = "inputs/Day01.txt";

pub struct Day1Solution {}
impl Day1Solution {
    fn solve_part_one() -> Result<u64, SolutionError> {
        let depth_measures = read_input(FILENAME)?;
        if depth_measures.first().is_none() {
            return Err(SolutionError {
                reason: stringify!("no depth measures found in input file; invalid.")
                    .parse()
                    .unwrap(),
            });
        }
        let first = depth_measures.first().unwrap();
        let mut prev: u64 = *first;
        let mut retval = 0;
        for measure in depth_measures {
            if measure > prev {
                retval += 1;
            }
            prev = measure;
        }
        Ok(retval)
    }
}
impl Solution for Day1Solution {
    fn solve(&self) -> Result<u64, SolutionError> {
        let depth_measures: Vec<u64> = read_input(FILENAME)?;

        if depth_measures.first().is_none() {
            return Err(SolutionError {
                reason: stringify!("no depth measures found in input file; invalid.")
                    .parse()
                    .unwrap(),
            });
        }
        if depth_measures.len() < 3 {
            return Ok(0);
        }
        //two solutions here,
        //#1 we could do one run through,
        //creating a new vec that has the windows for each sums,
        //then call our solution from above.
        //alternatively, we can just create a modified algorithm that's more performant
        //that monitors current progress through the window.
        //sooo.. let's do that! :)
        let mut ret_val = 0;
        let mut prev = depth_measures[0] + depth_measures[1] + depth_measures[2];
        for i in 3..depth_measures.len() {
            let new_sum = depth_measures[i] + depth_measures[i - 1] + depth_measures[i - 2];
            if new_sum > prev {
                ret_val += 1;
            }
            prev = new_sum;
        }
        return Ok(ret_val);
    }
}
